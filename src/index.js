
import React from 'react';
import ReactDOM from 'react-dom';

import '../src/styles/App.css';
import App from '../src/components/App';

ReactDOM.render( <App /> , document.getElementById('root') );